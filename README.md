# Kviz

## Autor: Ivan Hajmiler

## Upute:

Kada pokrenete "quiz.py" otvara se izbornik u kojem je ponuđeno pet opcija te vas se pita da unesete jednu od ponuđenih opcija. Opciju birate unosom broja koji se nalazi pored svake opcije. 

Prva opcija je "Start the quiz" i pokreće se unosom broja 1. Ova opcija pokreće kviz. Prvo vas pita za unos imena, a potom ispisuje poruku dobrodošlice. Zatim se na ekranu pojavljuje pitanje s četiri ponuđena odgovora. Od vas se traži da unesete jedan od ponuđenih odgovora, poželjno točan odgovor. Odgovor unosite tako što upišete jedan od ponuđenih odgovora pri čemu ne morate paziti na mala ili velika slova. Nakon unosa pritisnite enter kako bi potvrdili odgovor. Nakon potvrde na ekranu se odmah ispisuje jeste li točno ili netočno odgovorili i nastavlja se na iduće pitanje. Kada odgovorite na sva pitanja na ekranu se ispiše vaš rezultat (broj točnih odgovora u odnosu na ukupan broj pitanja).

Druga opcija je "Add questions to quiz" i pokreće se unosom broja 2. Ova opcija vas vodi kroz proces stvaranja pitanja i četiri ponuđena odgovara te točnog odgovora. Na ovaj način vi kreirate svoj kviz ili dodajete pitanja na već postojeći kviz. Ovisno koliko pitanja želite kreirati toliko puta će te iz glavnog izbornika pristupiti ovoj opciji.

Treća opcija je "Make new quiz" i pokreće se unosm broja 3. Ova opcija briše trenutni kviz i omogućuje stvaranje novog kviza od samog početka pri čemu pitanja unosimo kako smo rekli u prethodnom koraku.

Četvrta opcija je "Hall of fame" i pokreće se unosom broja 4. Ova opcija ispisuje na ekranu 10 najboljih igrača i njihove rezultate.

Peta opcija je "Exit" i pokreće se unosom broja 5. Ova opcija omogućuje izlaz iz aplikacije quiz.py.



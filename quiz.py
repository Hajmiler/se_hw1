def printMenu():     
    
    print (20 * "-" , "MENU" , 20 * "-")
    print ("1. Start the quiz")
    print ("2. Add questions to quiz")
    print ("3. Make new quiz")
    print ("4. Hall of fame")
    print ("5. Exit")
    print (47 * "-")



def startTheQuiz():
    
    score=0 
    counter=0
    numberOfquestions=0
    
    name=input("Enter your name: ")

    print()
    print("Hello "+name)
    print("Good luck")
    print()

    questions=open("questions.txt","r")
    
    for line in questions:
        counter+=1
        if counter<6:
            print(line.rstrip('\n'))
        else:
            counter=0
            numberOfquestions+=1
            print ()
            
            answer=input("Enter the answer: ").lower()
            
            if answer==line.rstrip("\n").lower():
                score+=1
                print("Correct answer\n")
            else:
                print("Incorrect answer\n")
                
    print("Your score is "+str(score)+" out of "+str(numberOfquestions))
    
    questions.close()
    
    
    resultsFile=open("results.txt","a")
    
    resultsFile.write(name+"\n"+str(score)+"\n")
    
    resultsFile.close()



def addQuestionsToQuiz():
    
    questions=open("questions.txt","a")
    
    
    yourQuestion=input("Enter the question: ")
    questions.write(yourQuestion+"\n")

    offeredAnswer1=input("Enter the first offered answer: ")
    questions.write(offeredAnswer1+"\n")

    offeredAnswer2=input("Enter the second offered answer: ")
    questions.write(offeredAnswer2+"\n")

    offeredAnswer3=input("Enter the third offered answer: ")
    questions.write(offeredAnswer3+"\n")

    offeredAnswer4=input("Enter the fourth offered answer: ")
    questions.write(offeredAnswer4+"\n")

    correctAnswer=input("Enter the correct ansswer: ")
    questions.write(correctAnswer+"\n")
    
    
    questions.close()



def makeNewQuiz():
    
    questions=open("questions.txt","w")
    questions.close()

    resultsFile=open("results.txt","w")
    resultsFile.close()



def displayHallOfFame():
    
    resultsFile=open("results.txt","r")
    
    results={}
    counter=0
    playerName=""
    
    for line in resultsFile:
        counter=counter+1
        if (counter%2!=0):
            playerName=line.rstrip("\n")
        else:
            playerResult=int(line.rstrip("\n"))
            results[playerName]=playerResult
            
    counter1=0
    for playerName, playerResult in sorted(results.items(), reverse=True, key=lambda x: x[1]):
        if (counter1<10):
            print (playerName, playerResult)
            counter1=counter1+1
            
    resultsFile.close()



def main():
    
    loop=True      
    
    while loop:          
        
        printMenu()    
        choice = str(input("Enter your choice [1-5]: "))
        
        if choice=='1':
            startTheQuiz()
            
        elif choice=='2':
            addQuestionsToQuiz()
    
        elif choice=='3':
            makeNewQuiz()

        elif choice=='4':
            displayHallOfFame()

        elif choice=='5':
            print ("Quit")
            loop=False

        else:
            input("Wrong option selected. Enter any key to try again..")


main()
